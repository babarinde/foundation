bricklayer
================

Base docker image to run PHP applications on Nginx with php-fpm with root directory as public/


Building the base image
-----------------------

To create the base image `odewumibabarinde/bricklayer`, execute the following command on the bricklayer folder:

    docker build -t odewumibabarinde/bricklayer .


Running your Nginx+PHP docker image
------------------------------------

Start your image binding the external ports 80 in all interfaces to your container:

    docker run -d -p 80:80 odewumibabarinde/bricklayer

Test your deployment:

    curl http://localhost/

Hello world!
...


Loading your custom PHP application
-----------------------------------

This image can be used as a base image for your PHP application. Create a new `Dockerfile` in your 
PHP application folder with the following contents:

    FROM odewumibabarinde/bricklayer

After that, build the new `Dockerfile`:

    docker build -t username/my-php-app .

And test it:

    docker run -d -p 80:80 username/my-php-app

Test your deployment:

    curl http://localhost/

That's it!


Loading your custom PHP application
--------------------------------------------------------------

Create a Dockerfile like the following:

    FROM odewumibabarinde/bricklayer
    ADD . /app
    
- Add your php application to `/app`

