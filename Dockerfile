FROM ubuntu:14.04
 
MAINTAINER Odewumi Babarinde Ayodeji "odewumibabrinde@abbaandking.com"
 
RUN apt-get update
RUN apt-get install -y postfix nginx
RUN apt-get install -y php5-cli php5-common php5-mongo php5-imagick php5-gd php5-intl php5-mysql php5-sqlite php5-curl php5-fpm
 
ADD . /app
RUN touch /var/log/php5-fpm.log
RUN chown -R www-data:www-data /var/log/php5-fpm.log
ADD default /etc/nginx/sites-enabled/default
RUN echo "cgi.fix_pathinfo = 0;" >> /etc/php5/fpm/php.ini
RUN echo "php_admin_value[display_errors] = 'stderr';" >> /etc/php5/fpm/php-fpm.conf
RUN echo "catch_workers_output = yes;" >> /etc/php5/fpm/php-fpm.conf
RUN echo "clear_env = no;" >> /etc/php5/fpm/php-fpm.conf
  
RUN echo "daemon off;" >> /etc/nginx/nginx.conf
 
EXPOSE 80
 
CMD service php5-fpm start && nginx

